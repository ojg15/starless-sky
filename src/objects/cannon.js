import { Projectile } from "../objects/projectile";
import { GameScene } from "../scenes/game";

export class Cannon extends Phaser.GameObjects.Image {

    static get cooldown() { return 500; }

    constructor(scene, x, y) {
        super(scene, x, y);
        this.setTexture("cannon");
        this.setPosition(x, y);

        this.canShoot = -1;
        this.balls = [];
    }

    _update(scene, delta, mousePos) {
        let angle = Phaser.Math.Angle.Between(this.x, this.y, mousePos.x, mousePos.y);
        this.rotation = angle + Math.PI / 2;

        if (this.canShoot < 0) {
            if (scene.input.activePointer.isDown) {
                this.canShoot = Cannon.cooldown;
                let direction = new Phaser.Geom.Point( Math.cos(angle), Math.sin(angle) );

                let ball = new Projectile(scene, this.x, this.y, direction);
                ball.depth = 90;
                scene.children.add(ball);
                this.balls.push(ball);
            }
        } else {
            this.canShoot -= delta;
        }

        for (let i = this.balls.length - 1; i >= 0; i--) {
            this.balls[i]._update(delta);

            if (this.balls[i].x < 0 || this.balls[i].x > GameScene.width ||
                this.balls[i].y < 0 || this.balls[i].y > GameScene.height) {

                this.balls[i].destroy();
                this.balls.splice(i, 1);
            }
        }
    }
}