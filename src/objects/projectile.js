import { GameScene } from "../scenes/game";

export class Projectile extends Phaser.GameObjects.Image {

    constructor(scene, x, y, direction) {
        super(scene, x, y);
        this.setTexture("bullet");
        this.setPosition(x, y);

        this._direction = direction;
        this._speed = 800;
    }

    _update(delta) {
        this.x += this._speed * this._direction.x * (delta / 1000);
        this.y += this._speed * this._direction.y * (delta / 1000);
    }
}