export class Ship extends Phaser.Physics.Arcade.Image {

    static get boostStartTime() { return 1500; }

    constructor(scene, x, y) {
        super(scene, x, y, "ship");
        this.setPosition(x, y);

        this.cursors = scene.input.keyboard.createCursorKeys();
        this.physics = scene.physics;
    }

    _update(delta) {
        if (this.cursors.left.isDown) { this.setAngularVelocity(-400); }
        else if (this.cursors.right.isDown) { this.setAngularVelocity(400); }
        else { this.setAngularVelocity(0); }

        if (this.cursors.up.isDown) {
            this.physics.velocityFromRotation(this.rotation - Math.PI / 2, 250, this.body.acceleration);
        } else {
            this.setAcceleration(0);
        }
    }
}