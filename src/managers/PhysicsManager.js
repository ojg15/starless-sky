import { GameScene } from '../scenes/game';
import { Point } from '../objects/point';

export class PhysicsManager {

    static checkCollision(firedBall, balls, setInPlaceCallback, delta, lastChance) {
        let ctr = firedBall.sprite.getCenter();
        let minDist = GameScene.ballDiametre * 0.85;
        let collided = null;
        for (let i = 0; i < balls.length; i++) {
            for (let j = 0; j < GameScene.numBallsPerRow; j++) {
                if (balls[i][j]) {
                    let colCtr = balls[i][j].sprite.getCenter();
                    let dist = Math.sqrt(
                        Math.pow((colCtr.x - ctr.x), 2) + Math.pow((colCtr.y - ctr.y), 2)
                    );
                    if (dist < minDist) {
                        minDist = dist;
                        collided = balls[i][j];
                    }
                }
            }
        }

        if (collided) {
            let colCtr = collided.sprite.getCenter();
            let i, j;
            let colInset = (collided.y + GameScene.addedRows) % 2 === 1;
            let surrounds = [
                new Point(colInset ? collided.x + 1: collided.x, collided.y + 1),
                new Point(colInset ? collided.x + 1: collided.x, collided.y - 1),
                new Point(collided.x + 1, collided.y),
                new Point(colInset ? collided.x: collided.x - 1, collided.y + 1),
                new Point(colInset ? collided.x: collided.x - 1, collided.y - 1),
                new Point(collided.x - 1, collided.y),
            ];

            if (ctr.x - colCtr.x > 0) {
                if (ctr.y - colCtr.y > GameScene.ballDiametre * 0.2) {
                    i = surrounds[0].y;
                    j = surrounds[0].x;
                    console.log(`Bottom right: ${surrounds[0]}`);
                } else if (ctr.y - colCtr.y < GameScene.ballDiametre * -0.2) {
                    i = surrounds[1].y;
                    j = surrounds[1].x;
                    console.log(`Top right: ${surrounds[1]}`);
                } else {
                    if (balls[surrounds[2].y] && balls[surrounds[2].y][surrounds[2].x]) {
                        i = surrounds[0].y;
                        j = surrounds[0].x;
                        console.log(`Adj. Bottom right: ${surrounds[0]}`);
                    } else {
                        i = surrounds[2].y;
                        j = surrounds[2].x;
                        console.log(`Direct Right: ${surrounds[2]}`);
                    }
                }
            } else {
                if (ctr.y - colCtr.y > GameScene.ballDiametre * 0.2) {
                    i = surrounds[3].y;
                    j = surrounds[3].x;
                    console.log(`Bottom Left: ${surrounds[3]}`);
                } else if (ctr.y - colCtr.y < GameScene.ballDiametre * -0.2) {
                    i = surrounds[4].y;
                    j = surrounds[4].x;
                    console.log(`Top Left: ${surrounds[4]}`);
                } else {
                    if (balls[surrounds[5].y] && balls[surrounds[5].y][surrounds[5].x]) {
                        i = surrounds[3].y;
                        j = surrounds[3].x;
                        console.log(`Adj. Bottom Left: ${surrounds[3]}`);
                    } else {
                        i = surrounds[5].y;
                        j = surrounds[5].x;
                        console.log(`Direct Left: ${surrounds[5]}`);
                    }
                }
            }

            if (i < 0) { i = 0; }

            if (!i) {
                console.warn(`Couldn't find the position, collided with: ${collided}`);
            } else if (balls[i] && balls[i][j]) {
                if (lastChance) {
                    while (balls[i] && balls[i][j]) { i++; }
                    setInPlaceCallback(i, j);
                }
                console.log(`Rolling back slightly to find open position`);
                firedBall.travel(delta * -0.5);
                PhysicsManager.checkCollision(firedBall, balls, setInPlaceCallback, delta, true);
            } else {
                setInPlaceCallback(i, j);
            }
        }
    }
}