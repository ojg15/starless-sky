import { Ship } from "../objects/ship";
import { Cannon } from "../objects/Cannon";

export class GameScene extends Phaser.Scene {

    static get width() { return 3840; }// window.innerWidth; }
    static get height() { return 2160; }// window.innerHeight; }

    constructor() {
        super();
        this.gameRunning = true;
        this.player = null;
        this.canShoot = true;
    }

    preload() {
        this.load.image('bg', 'assets/sky_quarter.png');
        this.load.image('cannon', 'assets/cannon.png');
        this.load.image('bullet', 'assets/bullet.png');
        this.load.image('ship', 'assets/ship.png');
    }

    create() {
        this.cameras.main.setBounds(0, 0, 1920 * 2, 1080 * 2);
        this.physics.world.setBounds(0, 0, 1920 * 2, 1080 * 2);
    
        this.bg1 = this.add.image(0, 0, 'bg').setOrigin(0).depth = -100;
        this.bg2 = this.add.image(1920, 0, 'bg').setOrigin(0).setFlipX(true).depth = -100;
        this.bg3 = this.add.image(0, 1080, 'bg').setOrigin(0).setFlipY(true).depth = -100;
        this.bg4 = this.add.image(1920, 1080, 'bg').setOrigin(0).setFlipX(true).setFlipY(true).depth = -100;

        let temp = new Ship(this, window.innerWidth * 0.5, window.innerHeight * 0.5);
        temp.setOrigin(0.5, 0.5);
        temp.depth = 10;
        this.player = this.physics.add.existing(temp);

        this.player.setCollideWorldBounds(true);
        this.player.setDamping(true);
        this.player.setDrag(0.99);
        this.player.setMaxVelocity(250);

        this.children.add(this.player);
        this.cameras.main.startFollow(this.player, true, 0.05, 0.05);

        this.cannon = new Cannon(this, this.player.x, this.player.y).setOrigin(0.5, 0.75);
        this.cannon.depth = 100;

        this.children.add(this.cannon);
    }

    update(time, delta) {

        if (!this.gameRunning) { return; }

        let mousePos = this.input.activePointer.positionToCamera(this.cameras.main);

        this.player._update(delta);

        this.cannon.x = this.player.x;
        this.cannon.y = this.player.y;
        this.cannon._update(this, delta, mousePos);
    }
}