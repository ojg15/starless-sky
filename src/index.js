import 'phaser';

import { GameScene } from './scenes/game';

const gameConfig = {
    width: window.innerWidth,
    height: window.innerHeight,
    physics: {
        default: "arcade",
        arcade: {
            fps: 60,
            gravity: { y: 0 }
        }
    },
    scene: GameScene
};

new Phaser.Game(gameConfig);